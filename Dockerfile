ARG FROM_IMAGE=gitlab-registry.cern.ch/cms-cloud/cmssw-docker/al9-cms:latest
FROM --platform=amd64 ${FROM_IMAGE}

ARG CMSSW_VERSION
ARG COMBINE_TAG
ARG HARVESTER_TAG

USER cmsusr
WORKDIR /home/cmsusr

ADD --chown=cmsusr:cmsusr ${CMSSW_VERSION} /home/cmsusr/${CMSSW_VERSION}

RUN shopt -s expand_aliases && \
    source /cvmfs/cms.cern.ch/cmsset_default.sh && \
    cd ${CMSSW_VERSION}/src && \
    scram b ProjectRename && \
    cmsenv && \
    scram b -j$(nproc)

ENV CMSSW_VERSION ${CMSSW_VERSION}
ENV COMBINE_TAG ${COMBINE_TAG}
ENV HARVESTER_TAG ${HARVESTER_TAG}

RUN echo "source /cvmfs/cms.cern.ch/cmsset_default.sh" >> /home/cmsusr/.bashrc && \
    echo "cd ${CMSSW_VERSION}/src" >> /home/cmsusr/.bashrc && \
    echo "cmsenv" >> /home/cmsusr/.bashrc
